import GCDWebServer

final class RequestInterceptor {
    private lazy var webServer: GCDWebServer = {
        GCDWebServer.setLogLevel(4)
        let webServer = GCDWebServer()
        return webServer
    }()
    
    private lazy var session: URLSession = {
        let config = URLSessionConfiguration.default
        config.requestCachePolicy = .reloadRevalidatingCacheData
        config.urlCache = nil
        return URLSession.init(configuration: config)
    }()
    
    init() {
        setupWebService()
        startWebService()
    }
    
    deinit {
        stopWebService()
    }
}

// MARK:- URL Modify
extension RequestInterceptor {
    private func makeOriginURL(_ url: URL) -> URL? {
        var components = URLComponents(string: url.absoluteString)
        components?.port = nil
        components?.host = originalHost
        components?.scheme = originalScheme
        return components?.url
    }
}

// MARK:- Web Service
extension RequestInterceptor {
    private func setupWebService() {
        webServer.addDefaultHandler(forMethod: "GET", request: GCDWebServerRequest.self) { (request, completion) in
            print("Requesting", request.url.absoluteString)
            let response = GCDWebServerDataResponse(html: "<html><body><p>Hello World</p></body></html>")
            completion(response)
        }
        /// Catching the m3u8 requests
        webServer.addHandler(forMethod: "GET", pathRegex: "/*.m3u8", request: GCDWebServerRequest.self) { [weak self] request, completion in
            print(request.url)
            guard let originURL = self?.makeOriginURL(request.url) else {
                let response = GCDWebServerDataResponse(text: "Unable to get orgin url")
                response?.statusCode = 400
                completion(response)
                return
            }
            /// To catch the ts requests, must download the data of m3u8 right here instead of redirect
            self?.session.dataTask(with: originURL) { (data, httpResponse, error) in
                let response: GCDWebServerDataResponse?
                defer {
                    completion(response)
                }
                if let error = error {
                    response = GCDWebServerDataResponse(text: error.localizedDescription)
                    response?.statusCode = 500
                    return
                }
                guard let res = httpResponse as? HTTPURLResponse else {
                    response = GCDWebServerDataResponse(text: "Invalid reponse")
                    response?.statusCode = 500
                    return
                }
                guard let data = data else {
                    response = GCDWebServerDataResponse(text: "Invalid data")
                    response?.statusCode = 500
                    return
                }
                response = GCDWebServerDataResponse(data: data, contentType: "")
                response?.statusCode = res.statusCode
            }.resume()
        }
        /// Catching the ts requests
        webServer.addHandler(forMethod: "GET", pathRegex: "/*.ts", request: GCDWebServerRequest.self) { [weak self] request, completion in
            print(request.url)
            var response: GCDWebServerResponse?
            guard let self = self else {
                response = GCDWebServerDataResponse(text: "Object has released")
                response?.statusCode = 500
                completion(response)
                return
            }
            guard let originURL = self.makeOriginURL(request.url) else {
                response = GCDWebServerDataResponse(text: "Unable to get orgin url")
                response?.statusCode = 400
                completion(response)
                return
            }
            /// To catch the ts requests, must download the data of m3u8 right here instead of redirect
            self.session.dataTask(with: originURL) { (data, httpResponse, error) in
                let response: GCDWebServerDataResponse?
                defer {
                    completion(response)
                }
                if let error = error {
                    response = GCDWebServerDataResponse(text: error.localizedDescription)
                    response?.statusCode = 500
                    return
                }
                guard let res = httpResponse as? HTTPURLResponse else {
                    response = GCDWebServerDataResponse(text: "Invalid reponse")
                    response?.statusCode = 500
                    return
                }
                guard let data = data else {
                    response = GCDWebServerDataResponse(text: "Invalid data")
                    response?.statusCode = 500
                    return
                }
                response = GCDWebServerDataResponse(data: data, contentType: "")
                response?.statusCode = res.statusCode
            }.resume()
        }
    }
    
    func startWebService() {
        if webServer.isRunning { stopWebService() }
        webServer.start(withPort: UInt(31795), bonjourName: "\(String(describing: Self.self))_Start_Web_Server")
        print("Web Server started with url:", webServer.serverURL?.absoluteString ?? "empty_url")
    }
    
    func stopWebService() {
        if webServer.isRunning {
            webServer.stop()
            webServer.removeAllHandlers()
        }
        print("Web Server stopped")
    }
}
