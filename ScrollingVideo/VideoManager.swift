import AVFoundation

protocol VideoManagerDataSource: NSObjectProtocol {
    func videoManager(_ videoManger: VideoManager, urlAt index: Int) -> URL?
}

extension VideoManager {
    class VideoCache {
        let url: URL
        let cachedTime: TimeInterval
        let player: AVPlayer
        
        init(url: URL, cachedTime: TimeInterval, player: AVPlayer) {
            self.url = url
            self.cachedTime = cachedTime
            self.player = player
        }
    }
}

final class VideoManager {
    private let numPreloadVideoAfterFocusedVideo = 2
    private let numPreloadVideosBeforeFocusedVideo = 2
    private var maxVideosAllowed: Int {
        numPreloadVideoAfterFocusedVideo + numPreloadVideosBeforeFocusedVideo + 1
    }
    
    private lazy var videoCachedMap = [URL: VideoCache]()
    
    private weak var dataSource: VideoManagerDataSource?
    init(dataSource: VideoManagerDataSource) {
        self.dataSource = dataSource
    }
}

// MARK:- Private
extension VideoManager {
    @discardableResult
    private func makePlayer(at index: Int) -> AVPlayer? {
        guard let url = dataSource?.videoManager(self, urlAt: index) else { return nil }
        return makePlayer(for: url)
    }
    
    private func makePlayer(for url: URL) -> AVPlayer {
        if let videoCache = videoCachedMap[url] {
            print("Reuse player for:", url)
            //videoCache.player.seek(to: CMTime.zero)
//            videoCache.player.currentItem?.seek(to: .zero)
            return videoCache.player
        } else {
            //removeVideoIfMaxNumberOfVideosReached()
            print("New player for:", url)
            let playerItem = AVPlayerItem(url: url)
            playerItem.preferredForwardBufferDuration = 0
            let player = AVPlayer(playerItem: playerItem)
            let videoCache = VideoCache(
                url: url,
                cachedTime: Date().timeIntervalSince1970,
                player: player
            )
            videoCachedMap[url] = videoCache
            return player
        }
    }
    
    private func removeVideoIfMaxNumberOfVideosReached() {
        if videoCachedMap.count >= maxVideosAllowed {
            // Remove the least recently used video
            let sortedVideoCached = videoCachedMap.values.sorted(by: { $0.cachedTime > $1.cachedTime })
            guard let leastRecentlyVideoCache = sortedVideoCached.first else {
                return
            }
            print("Remove cache for: \(leastRecentlyVideoCache.url)")
            videoCachedMap.removeValue(forKey: leastRecentlyVideoCache.url)
        }
    }
    
    private func preloadVideos(with focusIndex: Int) {
        preloadVideosAfterFocusedVideo(with: focusIndex)
        preloadVideosBeforeFocusedVideo(with: focusIndex)
    }
    
    private func preloadVideosAfterFocusedVideo(with focusedIndex: Int) {
        guard numPreloadVideoAfterFocusedVideo > 0 else { return }
        for i in 1...numPreloadVideoAfterFocusedVideo {
            print("Preload after \(focusedIndex + i)")
            makePlayer(at: focusedIndex + i)
        }
    }
    
    private func preloadVideosBeforeFocusedVideo(with focusedIndex: Int) {
        guard numPreloadVideosBeforeFocusedVideo > 0 else { return }
        for i in 1...numPreloadVideosBeforeFocusedVideo {
            print("Preload before \(focusedIndex - i)")
            makePlayer(at: focusedIndex - i)
        }
    }
}

// MARK:- Public
extension VideoManager {
    func player(at index: Int) -> AVPlayer? {
        print("Get player for:", index)
        preloadVideos(with: index)
        return makePlayer(at: index)
    }
}
