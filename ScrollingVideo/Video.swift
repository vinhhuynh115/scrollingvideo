import Foundation

struct Video {
    let id: String
    let title: String
    let url: URL
}

extension Video: Equatable {
    static func ==(lhs: Video, rhs: Video) -> Bool {
        lhs.id == rhs.id
    }
}
