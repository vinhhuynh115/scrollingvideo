import Foundation

let localScheme = "http"
let localHost = "127.0.0.1"
let localPort = UInt(31795)

let originalScheme = "https"
let originalHost = "vod07-cdn.fptplay.net"

let originURLs = [
    URL(string: "https://vod07-cdn.fptplay.net/ovod/_definst_/mp4/encoded/20211212/if_the_voice_has_memory_cn_2021_01_1080p/d4d154cdda1aef316e8a45a281996e8a_720p.mp4/chunklist.m3u8")!,
    URL(string: "https://vod07-cdn.fptplay.net/ovod/_definst_/mp4/encoded/20211213/if_the_voice_has_memory_cn_2021_v2_05_1080p/c8de637fb2961392b71c47ee67a4f49b_720p.mp4/chunklist.m3u8")!,
//    URL(string: "https://vod07-cdn.fptplay.net/ovod/_definst_/mp4/encoded/20211212/if_the_voice_has_memory_cn_2021_01_1080p/d4d154cdda1aef316e8a45a281996e8a_720p.mp4/chunklist.m3u8")!,
//    URL(string: "https://vod07-cdn.fptplay.net/ovod/_definst_/mp4/encoded/20211213/if_the_voice_has_memory_cn_2021_v2_05_1080p/c8de637fb2961392b71c47ee67a4f49b_720p.mp4/chunklist.m3u8")!,
//    URL(string: "https://vod07-cdn.fptplay.net/ovod/_definst_/mp4/encoded/20211212/if_the_voice_has_memory_cn_2021_01_1080p/d4d154cdda1aef316e8a45a281996e8a_720p.mp4/chunklist.m3u8")!,
//    URL(string: "https://vod07-cdn.fptplay.net/ovod/_definst_/mp4/encoded/20211213/if_the_voice_has_memory_cn_2021_v2_05_1080p/c8de637fb2961392b71c47ee67a4f49b_720p.mp4/chunklist.m3u8")!,
//    URL(string: "https://vod07-cdn.fptplay.net/ovod/_definst_/mp4/encoded/20211212/if_the_voice_has_memory_cn_2021_01_1080p/d4d154cdda1aef316e8a45a281996e8a_720p.mp4/chunklist.m3u8")!,
//    URL(string: "https://vod07-cdn.fptplay.net/ovod/_definst_/mp4/encoded/20211213/if_the_voice_has_memory_cn_2021_v2_05_1080p/c8de637fb2961392b71c47ee67a4f49b_720p.mp4/chunklist.m3u8")!,
//    URL(string: "https://vod07-cdn.fptplay.net/ovod/_definst_/mp4/encoded/20211212/if_the_voice_has_memory_cn_2021_01_1080p/d4d154cdda1aef316e8a45a281996e8a_720p.mp4/chunklist.m3u8")!,
//    URL(string: "https://vod07-cdn.fptplay.net/ovod/_definst_/mp4/encoded/20211213/if_the_voice_has_memory_cn_2021_v2_05_1080p/c8de637fb2961392b71c47ee67a4f49b_720p.mp4/chunklist.m3u8")!,
//    URL(string: "https://vod07-cdn.fptplay.net/ovod/_definst_/mp4/encoded/20211212/if_the_voice_has_memory_cn_2021_01_1080p/d4d154cdda1aef316e8a45a281996e8a_720p.mp4/chunklist.m3u8")!,
//    URL(string: "https://vod07-cdn.fptplay.net/ovod/_definst_/mp4/encoded/20211213/if_the_voice_has_memory_cn_2021_v2_05_1080p/c8de637fb2961392b71c47ee67a4f49b_720p.mp4/chunklist.m3u8")!,
//    URL(string: "https://vod07-cdn.fptplay.net/ovod/_definst_/mp4/encoded/20211212/if_the_voice_has_memory_cn_2021_01_1080p/d4d154cdda1aef316e8a45a281996e8a_720p.mp4/chunklist.m3u8")!,
//    URL(string: "https://vod07-cdn.fptplay.net/ovod/_definst_/mp4/encoded/20211213/if_the_voice_has_memory_cn_2021_v2_05_1080p/c8de637fb2961392b71c47ee67a4f49b_720p.mp4/chunklist.m3u8")!,
//    URL(string: "https://vod07-cdn.fptplay.net/ovod/_definst_/mp4/encoded/20211212/if_the_voice_has_memory_cn_2021_01_1080p/d4d154cdda1aef316e8a45a281996e8a_720p.mp4/chunklist.m3u8")!,
//    URL(string: "https://vod07-cdn.fptplay.net/ovod/_definst_/mp4/encoded/20211213/if_the_voice_has_memory_cn_2021_v2_05_1080p/c8de637fb2961392b71c47ee67a4f49b_720p.mp4/chunklist.m3u8")!,
//    URL(string: "https://vod07-cdn.fptplay.net/ovod/_definst_/mp4/encoded/20211212/if_the_voice_has_memory_cn_2021_01_1080p/d4d154cdda1aef316e8a45a281996e8a_720p.mp4/chunklist.m3u8")!,
//    URL(string: "https://vod07-cdn.fptplay.net/ovod/_definst_/mp4/encoded/20211213/if_the_voice_has_memory_cn_2021_v2_05_1080p/c8de637fb2961392b71c47ee67a4f49b_720p.mp4/chunklist.m3u8")!,
//    URL(string: "https://vod07-cdn.fptplay.net/ovod/_definst_/mp4/encoded/20211212/if_the_voice_has_memory_cn_2021_01_1080p/d4d154cdda1aef316e8a45a281996e8a_720p.mp4/chunklist.m3u8")!,
//    URL(string: "https://vod07-cdn.fptplay.net/ovod/_definst_/mp4/encoded/20211213/if_the_voice_has_memory_cn_2021_v2_05_1080p/c8de637fb2961392b71c47ee67a4f49b_720p.mp4/chunklist.m3u8")!,
]

//    URL(string: "https://vod07-cdn.fptplay.net/ovod/_definst_/mp4/encoded/20211212/if_the_voice_has_memory_cn_2021_01_1080p/d4d154cdda1aef316e8a45a281996e8a_720p.mp4/chunklist.m3u8")!,
//    URL(string: "https://vod05-cdn.fptplay.net/ovod/_definst_/mp4/encoded/20211211/if_the_voice_has_memory_cn_2021_02_1080p/819a36d0e0509f48f609f203f86e8973_720p.mp4/chunklist.m3u8")!,
//    URL(string: "https://vod02-cdn.fptplay.net/ovod/_definst_/mp4/encoded/20211211/if_the_voice_has_memory_cn_2021_03_1080p/53a85bab0406800b84c7d9b62d691429_720p.mp4/chunklist.m3u8")!,
//    URL(string: "https://vod01-cdn.fptplay.net/ovod/_definst_/mp4/encoded/20211211/if_the_voice_has_memory_cn_2021_04_1080p/5e3308609ec2007fb412619028ce5bde_720p.mp4/chunklist.m3u8")!,
//    URL(string: "https://vod07-cdn.fptplay.net/ovod/_definst_/mp4/encoded/20211213/if_the_voice_has_memory_cn_2021_v2_05_1080p/c8de637fb2961392b71c47ee67a4f49b_720p.mp4/chunklist.m3u8")!,
//    URL(string: "https://vod01-cdn.fptplay.net/ovod/_definst_/mp4/encoded/20211213/if_the_voice_has_memory_cn_2021_v2_06_1080p/8579d2feee342796fb845a2a5991aa50_720p.mp4/chunklist.m3u8")!,
//    URL(string: "https://vod05-cdn.fptplay.net/ovod/_definst_/mp4/encoded/20211213/if_the_voice_has_memory_cn_2021_v2_07_1080p/b8eb6ac467db25886cdd269b8525bd89_720p.mp4/chunklist.m3u8")!,
//    URL(string: "https://vod07-cdn.fptplay.net/ovod/_definst_/mp4/encoded/20211212/if_the_voice_has_memory_cn_2021_01_1080p/d4d154cdda1aef316e8a45a281996e8a_720p.mp4/chunklist.m3u8")!,
//    URL(string: "https://vod05-cdn.fptplay.net/ovod/_definst_/mp4/encoded/20211211/if_the_voice_has_memory_cn_2021_02_1080p/819a36d0e0509f48f609f203f86e8973_720p.mp4/chunklist.m3u8")!,
//    URL(string: "https://vod02-cdn.fptplay.net/ovod/_definst_/mp4/encoded/20211211/if_the_voice_has_memory_cn_2021_03_1080p/53a85bab0406800b84c7d9b62d691429_720p.mp4/chunklist.m3u8")!,
//    URL(string: "https://vod01-cdn.fptplay.net/ovod/_definst_/mp4/encoded/20211211/if_the_voice_has_memory_cn_2021_04_1080p/5e3308609ec2007fb412619028ce5bde_720p.mp4/chunklist.m3u8")!,
//    URL(string: "https://vod07-cdn.fptplay.net/ovod/_definst_/mp4/encoded/20211213/if_the_voice_has_memory_cn_2021_v2_05_1080p/c8de637fb2961392b71c47ee67a4f49b_720p.mp4/chunklist.m3u8")!,
//    URL(string: "https://vod01-cdn.fptplay.net/ovod/_definst_/mp4/encoded/20211213/if_the_voice_has_memory_cn_2021_v2_06_1080p/8579d2feee342796fb845a2a5991aa50_720p.mp4/chunklist.m3u8")!,
//    URL(string: "https://vod05-cdn.fptplay.net/ovod/_definst_/mp4/encoded/20211213/if_the_voice_has_memory_cn_2021_v2_07_1080p/b8eb6ac467db25886cdd269b8525bd89_720p.mp4/chunklist.m3u8")!,
//    URL(string: "https://vod07-cdn.fptplay.net/ovod/_definst_/mp4/encoded/20211212/if_the_voice_has_memory_cn_2021_01_1080p/d4d154cdda1aef316e8a45a281996e8a_720p.mp4/chunklist.m3u8")!,
//    URL(string: "https://vod05-cdn.fptplay.net/ovod/_definst_/mp4/encoded/20211211/if_the_voice_has_memory_cn_2021_02_1080p/819a36d0e0509f48f609f203f86e8973_720p.mp4/chunklist.m3u8")!,
//    URL(string: "https://vod02-cdn.fptplay.net/ovod/_definst_/mp4/encoded/20211211/if_the_voice_has_memory_cn_2021_03_1080p/53a85bab0406800b84c7d9b62d691429_720p.mp4/chunklist.m3u8")!,
//    URL(string: "https://vod01-cdn.fptplay.net/ovod/_definst_/mp4/encoded/20211211/if_the_voice_has_memory_cn_2021_04_1080p/5e3308609ec2007fb412619028ce5bde_720p.mp4/chunklist.m3u8")!,
//    URL(string: "https://vod07-cdn.fptplay.net/ovod/_definst_/mp4/encoded/20211213/if_the_voice_has_memory_cn_2021_v2_05_1080p/c8de637fb2961392b71c47ee67a4f49b_720p.mp4/chunklist.m3u8")!,
//    URL(string: "https://vod01-cdn.fptplay.net/ovod/_definst_/mp4/encoded/20211213/if_the_voice_has_memory_cn_2021_v2_06_1080p/8579d2feee342796fb845a2a5991aa50_720p.mp4/chunklist.m3u8")!,
//    URL(string: "https://vod05-cdn.fptplay.net/ovod/_definst_/mp4/encoded/20211213/if_the_voice_has_memory_cn_2021_v2_07_1080p/b8eb6ac467db25886cdd269b8525bd89_720p.mp4/chunklist.m3u8")!,
