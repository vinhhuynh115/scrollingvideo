import UIKit
import AVFoundation

final class VideoTableViewCell: UITableViewCell {
    static let identifier = String(describing: VideoTableViewCell.self)
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var playerView: PlayerView!
    
    private var video: Video? {
        didSet {
            titleLabel.text = video?.title
        }
    }
    private lazy var isFocusing = false
    
//    override func prepareForReuse() {
//        super.prepareForReuse()
//        titleLabel.text = nil
//    }
    
    func fill(with video: Video) {
        self.video = video
    }
    
    func focus(with player: AVPlayer?) {
        guard !isFocusing else { return }
        isFocusing = true
        print("Focus \(video?.id ?? "Video unknown")")
        playerView.player = player
        player?.play()
    }
    
    func unfocus() {
        guard isFocusing else { return }
        isFocusing = false
        print("Unfocus \(video?.id ?? "Video unknown")")
        playerView.player?.pause()
        playerView.player = nil
    }
}
