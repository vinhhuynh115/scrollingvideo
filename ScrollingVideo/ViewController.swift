import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private let requestInterceptor = RequestInterceptor()
    private lazy var videoManager = VideoManager(dataSource: self)
    
    private lazy var videos = [Video]() {
        didSet {
            tableView.reloadData()
        }
    }
    
    private var latedFocusedIndexPath: IndexPath? {
        didSet {
            guard latedFocusedIndexPath != oldValue,
                  let latedFocusedIndexPath = latedFocusedIndexPath,
                  let cell = tableView.cellForRow(at: latedFocusedIndexPath) as? VideoTableViewCell else {
                      return
                  }
            print("=====")
            print("Focus index path: \(latedFocusedIndexPath.row)")
            let player = videoManager.player(at: latedFocusedIndexPath.row)
            cell.focus(with: player)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupItems()
        setupTableView()
        setupTableViewRow()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setupTableViewRow()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
}

// MARK:- Setting Up
extension ViewController {
    private func setupItems() {
//        let urls = [
//            URL(string: "http://playertest.longtailvideo.com/adaptive/oceans_aes/oceans_aes.m3u8")!,
//            URL(string: "http://playertest.longtailvideo.com/adaptive/captions/playlist.m3u8")!,
//            URL(string: "https://bitdash-a.akamaihd.net/content/MI201109210084_1/m3u8s/f08e80da-bf1d-4e3d-8899-f0f6155f6efa.m3u8")!,
//            URL(string: "https://multiplatform-f.akamaihd.net/i/multi/will/bunny/big_buck_bunny_,640x360_400,640x360_700,640x360_1000,950x540_1500,.f4v.csmil/master.m3u8")!,
//            URL(string: "http://devimages.apple.com/iphone/samples/bipbop/bipbopall.m3u8")!,
//            URL(string: "https://devstreaming-cdn.apple.com/videos/streaming/examples/img_bipbop_adv_example_fmp4/master.m3u8")!,
//        ]
        let localURLs = originURLs
            .map { url -> URL? in
                var components = URLComponents(string: url.absoluteString)
                components?.port = Int(localPort)
                components?.scheme = localScheme
                components?.host = localHost
                return components?.url
            }
            .compactMap { $0 }
        var videos = [Video]()
        for (index, url) in localURLs.enumerated() {
            let video = Video(id: "video_\(index)", title: "Video \(index)", url: url)
            videos.append(video)
        }
        self.videos = videos
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
            self?.latedFocusedIndexPath = IndexPath(row: 0, section: 0)
        }
    }
    
    private func setupTableView() {
        tableView.isPagingEnabled = true
        tableView.contentInsetAdjustmentBehavior = .never
        tableView.bounces = false
        if #available(iOS 15.0, *) {
            tableView.sectionHeaderTopPadding = 0
        }
    }
    
    private func setupTableViewRow() {
        tableView.rowHeight = tableView.bounds.height
    }
}

// MARK:- UITableViewDataSource
extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        videos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let video = videos[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: VideoTableViewCell.identifier, for: indexPath) as! VideoTableViewCell
        cell.fill(with: video)
        return cell
    }
}

// MARK:- UITableViewDelegate
extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let cell = cell as? VideoTableViewCell else { return }
        cell.unfocus()
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        scrollDidEnd()
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            scrollDidEnd()
        }
    }
    
    private func scrollDidEnd() {
        guard let focusedIndexPath = tableView.indexPathsForVisibleRows?.first else { return }
        latedFocusedIndexPath = focusedIndexPath
    }
}

// MARK:- VideoManagerDataSource
extension ViewController: VideoManagerDataSource {
    func videoManager(_ videoManger: VideoManager, urlAt index: Int) -> URL? {
        guard index >= 0, index < videos.count else { return nil }
        return videos[index].url
    }
}
