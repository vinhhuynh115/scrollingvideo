import UIKit
import AVFoundation

public class PlayerView: UIView {
    public var player: AVPlayer? {
        get {
            return playerLayer.player
        }
        set {
            playerLayer.player = newValue
        }
    }
    
    public var playerLayer: AVPlayerLayer {
        return layer as! AVPlayerLayer
    }
    
    public var borderWidth:CGFloat = 0
    
    public override class var layerClass : AnyClass {
        return AVPlayerLayer.self
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        
        if #available(iOS 13, *) {
            let maskLayer = playerLayer.mask ?? CALayer()
            maskLayer.frame = playerLayer.bounds.insetBy(dx: 0, dy: 0)
            maskLayer.backgroundColor = UIColor.black.cgColor
            playerLayer.mask = maskLayer
        }
    }
}
